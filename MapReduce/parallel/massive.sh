#!/bin/sh

if [ $# != 1 ]; then
	echo "usage: $0 <log file"
	exit 1
fi

cmd=${HADOOP_HOME}/bin/hadoop
rmcmd=${HADOOP_HOME}/bin/hdfs
jar=${HADOOP_HOME}/share/hadoop/tools/lib/hadoop-streaming-2.7.0.jar

for i in ./a.sh ./p.sh ./app.sh ./ppa.sh; do
	for j in 0 1 2; do
		echo "Invoking $i $j"
		/usr/bin/time -o $1 -a $i /home/lyznardh/input /home/lyznardh/output-$j
	done
	${rmcmd} dfs -rm -r -skipTrash /home/lyznardh/output-*
done
