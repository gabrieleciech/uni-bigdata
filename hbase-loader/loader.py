#!/usr/bin/python
# encoding: utf-8

from __future__ import print_function
import sys
import json

def scan (filename):
	with open(filename, 'r') as ifp:
		for entry in json.load(ifp, 'utf-8').get('d').get('results'):
			yield(entry.get('PaperID'), entry.get('AuthorID'), entry.get('SeqID'))

def main ():
	for name in sys.argv[1:]:
		for entry in scan(name):
			print("put 'pa', '%s', 'a:%s', '%s'" % entry)

if __name__ == '__main__':
	main()
