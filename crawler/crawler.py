#!/usr/bin/python
# enconding: utf-8

from __future__ import print_function
import urllib2
import os
import time
import random
import sys
import xml.etree.ElementTree as ET

def crawl (tablename, start_counter, start_req):

	print('Now crawling table %s' % tablename)
	serviceroot = 'https://api.datamarket.azure.com/MRC/MicrosoftAcademic/v2/'
	if start_counter:
		counter = start_counter
	else: counter = 0

	tabledir = '%s.d' % tablename
	if not os.path.exists(tabledir):
		os.makedirs(tabledir)

	if start_req: req = start_req
	else: req = serviceroot + tablename
	logfile = open(os.path.join(tabledir, '%s.log' % tablename), 'w')

	while req:

		#wait = random.uniform(0.5, 1.0)
		#time.sleep(wait)

		if 'https://api.datamarket.azure.com/' not in req:
			print('Req %s does not match Serviceroot' % req, file=logfile)
			break

		xml = urllib2.urlopen(req).read()
		chunkfile = '%s_%06d.xml' % (tablename, counter)
		chunkfilename = os.path.join(tabledir, chunkfile)

		with open(chunkfilename, 'w') as of:
			print(xml, file=of)

		treeroot = ET.fromstring(xml.replace('&#', '&amp;#'))
		req = treeroot.findall('{http://www.w3.org/2005/Atom}link')[-1].attrib['href']
		print('Chunk #%06d %s written on %s' % (counter, req, chunkfilename), file=logfile)
		counter += 1

	print('Done', file=logfile)

argno = len(sys.argv) - 1

if argno == 1:
	crawl(sys.argv[1], False, False)
elif argno == 3:
	crawl(sys.argv[1], int(sys.argv[2]), sys.argv[3])
else:
	print('Please specify one or three arguments')

print('Done')

