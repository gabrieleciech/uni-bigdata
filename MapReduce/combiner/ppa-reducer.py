#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function
import sys

prev = None
count = 0
for line in sys.stdin:
	step, word = line.strip().split()
	if prev != word:
		if prev: print('%7d %s' % (count, prev))
		prev = word
		count = int(step)
	else:
		count += int(step)

print('%7d %s' % (count, prev))
