#!/usr/bin/python
# encoding: utf-8

from __future__ import print_function
import urllib2
import os
import socket
import sys
import datetime


def crawl(table_name=None, start_counter=0):

    # List of items to download
    # Pair(<Table>, <Order>)
    download_items = [
        ('Category', 'DomainID'),
        ('Keyword', 'ID'),
        ('Author', 'ID'),
        ('Paper_Keyword', 'CPaperID,KeywordID'),
        ('Paper_Category', 'CPaperID,DomainID'),
        ('Paper', 'ID'),
        ('Paper_Author', 'PaperID,AuthorID'),
        ('Paper_Ref', 'SrcID')
        # 'Affiliation': 'ID',
        # 'Conf_Category': 'ConfCategoryID',
        # 'Conference': 'ID',
        # 'Domain': 'ID',
        # 'Jour_Category': 'CJourID',
        # 'Journal': 'ID',
        # 'OrganizationsByGeography': 'ID',
        # 'Paper_Url': 'ID',
        # 'PaperAffiliationCount': 'paperCount'
    ]

    # Service root url
    service_root = 'https://api.datamarket.azure.com/MRC/MicrosoftAcademic/v2/'

    # Number of records to retrieve each call
    top_records = 10000

    # Loop over downloadable items
    for (table, order) in download_items:

        # If passing table name for recovery jump
        if table_name is not None and table_name != table:
            print(datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S") + ' - ' + 'Jumping table ' + table)
            continue

        print(datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S") + ' - ' + 'Now crawling table %s' % table)

        # Set up counters
        if start_counter:
            counter = start_counter
            skip_records = top_records*start_counter
            table_name = None
            start_counter = 0
        else:
            counter = 0
            skip_records = 0

        # Create dir for saving file
        table_dir = '%s.d' % table
        if not os.path.exists(table_dir):
            os.makedirs(table_dir)

        # Request url
        req = service_root + table + '?$format=json&$orderby=' + order + '&$top=' + str(top_records) + '&$skip=' + str(skip_records)

        # Log file
        log_filename = os.path.join(table_dir, '%s.log' % table)
        if os.path.exists(log_filename) and os.path.isfile(log_filename):
            append = True
        else:
            append = False

        logfile = open(log_filename, 'a' if append else 'w')

        while req:

            # Get json if empty break
            try:
                json = urllib2.urlopen(req, timeout=60*5).read()
            except Exception as e:
                print(datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S") + ' - ' + 'ERRORE')
                raise

            # Response is empty
            if json == '{"d":{"results":[]}}':
                break

            # Save file on fs
            chunk_file = '%s_%08d.json' % (table, counter)
            chunk_filename = os.path.join(table_dir, chunk_file)

            with open(chunk_filename, 'w') as of:
                print(json, file=of)

            # Log on file
            print(datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S") + ' - ' + 'Chunk #%06d %s written on %s' % (counter, req, chunk_filename), file=logfile)

            # Increment counter, skip_records to skip and prepare url for next request
            counter += 1
            skip_records += top_records
            req = service_root + table + '?$format=json&$orderby=' + order + '&$top=' + str(top_records) + '&$skip=' + str(skip_records)

        # Print done on log file
        print(datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S") + ' - ' + 'Done', file=logfile)


argno = len(sys.argv) - 1
socket.setdefaulttimeout(60*5)

if argno == 0:
    crawl()
elif argno == 2:
    crawl(sys.argv[1], int(sys.argv[2]))
else:
    print('Please specify two or none arguments')

print(datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S") + ' - ' + 'Done')
