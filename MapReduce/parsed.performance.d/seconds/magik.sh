#!/bin/bash

declare -a t=('a' 'p' 'app' 'ppa')

for i in `seq 0 3`; do
	d=../${t[$i]}.dat
	> $d
	for j in 1 2 4 10 20 40 100 200 400 1000; do
		printf "%8d " $(( $j * 10000 )) >> $d
		for f in *-$j.times; do
			v=$(head -n$(( $i + 1)) $f | tail -n1)
			printf "%15f" $v >> $d
		done
		echo >> $d
	done
done
