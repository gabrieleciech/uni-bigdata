#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function
import sys

prev = None
count = 0
for line in sys.stdin:
	word = line.strip()
	if prev != word:
		if prev: print('%7d %s' % (count, prev))
		prev = word
		count = 1
	else:
		count += 1

print('%7d %s' % (count, prev))
