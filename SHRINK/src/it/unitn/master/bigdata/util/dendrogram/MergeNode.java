/*
 * This file is licensed to You under the "Simplified BSD License".
 * You may not use this software except in compliance with the License. 
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/bsd-license.php
 * 
 * See the COPYRIGHT file distributed with this work for information
 * regarding copyright ownership.
 */
/*******************************************************************************
 * Disclaimer: code adapted from
 *    https://github.com/sape/hac/tree/master/src/ch/usi/inf/sape/hac/dendrogram
 ******************************************************************************/
package it.unitn.master.bigdata.util.dendrogram;

import java.util.Collection;


/**
 * A MergeNode represents an interior node in a Dendrogram.
 * It corresponds to a (non-singleton) cluster of observations.
 */
public final class MergeNode extends DendrogramNode {
        private static final boolean SHOW_EPS = true;
    
        private final double shrinking_eps;
	private final Collection<DendrogramNode> microcluster;
	private final int observationCount;
	
	
	public MergeNode(final Collection<DendrogramNode> microcluster, final double shrinking_eps) {
                super();
                this.shrinking_eps = shrinking_eps;
		this.microcluster = microcluster;

		int observationCount_ = 0;
                for (DendrogramNode elem: this.microcluster) {
                    observationCount_ += elem.getObservationCount();
                }
                this.observationCount = observationCount_;
	}
	
        @Override
	public int getObservationCount() {
		return observationCount;
	}

        private Double getShrinking_eps() {
            return shrinking_eps;
        }
        
        @Override
	public final Collection<DendrogramNode> getMicroCluster() {
		return microcluster;
	}

        private static String string_of_node(String indent, DendrogramNode node, boolean just_entered) {
            String outstr = "";
            String BEGIN = ((just_entered) ? indent : indent+"-->");
            if (node==null) {
                outstr = (BEGIN+"<null>");
            } else if (node instanceof ObservationNode) {
                outstr = (BEGIN+"Observation: "+node);
            } else if (node instanceof MergeNode) {                
                outstr += (BEGIN+"Merge:");
                for (DendrogramNode mergenode: node.getMicroCluster()) {
                    String eps = ((MergeNode.SHOW_EPS)
                                  ? (((MergeNode)node).getShrinking_eps()).toString()
                                  : ""
                                 );
                    String CONTINUATION = indent+((just_entered)
                                                  ? "  |"
                                                  : "---" + eps + "---");
                    outstr += string_of_node(CONTINUATION, mergenode, false);
                }                  
            }
            return ((just_entered) ? outstr : "\n"+outstr);
        }

        @Override
        public String toString() {
            return string_of_node("  ", this, true);
        }
       
}




