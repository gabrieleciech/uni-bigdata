/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.master.bigdata;

import it.unitn.master.bigdata.entities.Author;
import it.unitn.master.bigdata.graph.Graph;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author lyznardh
 */
public class Shrink {

    public Map<Integer, LinkedList<Author>> paperAuthorLstMap;
    public Map<Integer, Author> authorMap;
    public Graph graph;

//    public static void parseLine(
//            String line, DefaultDirectedWeightedGraph<String, DefaultWeightedEdge> graph)
//            throws IOException {
//
//        String[] token = line.split(" ");
//        if (token.length != 2) {
//            throw new IOException("Line [" + line + "] does not contain two token ");
//        }
//        //System.out.println("Paper["+token[0]+"], Author["+token[1]+"]");
//        String paper = "p" + token[0];
//        String author = "a" + token[1];
//        //System.out.println("Paper["+paper+"], Author["+author+"]");
//
//        graph.addVertex(paper);
//        graph.addVertex(author);
//
//        DefaultWeightedEdge existingEdge = graph.getEdge(paper, author);
//
//        if (existingEdge == null) {
//            // Add edge with default weight 1
//            DefaultWeightedEdge edge = graph.addEdge(paper, author);
//            graph.setEdgeWeight(edge, 1);
//        } else {
//            // Get the weight of the current one and modify it
//            double weight = graph.getEdgeWeight(existingEdge);
//            weight += 1;
//            graph.setEdgeWeight(existingEdge, weight);
//        }
//
//    }

    public Shrink() {
        this.paperAuthorLstMap = new HashMap<>();
        this.authorMap = new HashMap<>();
        this.graph = new Graph();
    }

    /**
     * Function used to import author from json files
     * @param directories list of paths
     */
    private void loadData(String[] directories) {

        // Loop over args
        for (String arg : directories) {

            File folder = new File(arg);
            File[] listOfFiles = folder.listFiles();

            for (File jsonFile : listOfFiles) {

                // If current file is dir skip
                if (jsonFile.isDirectory()) {
                    continue;
                }

                try {
                    // Parse Json file
                    JSONParser parser = new JSONParser();
                    Object obj = parser.parse(new FileReader(jsonFile));
                    JSONObject jsonObject = (JSONObject) obj;
                    JSONObject dJson = (JSONObject) jsonObject.get("d");
                    JSONArray results = (JSONArray) dJson.get("results");
                    // Loop over Json results array
                    Iterator iter = results.iterator();
                    while (iter.hasNext()) {

                        // Get author properties from JSON
                        JSONObject item = (JSONObject) iter.next();
                        int paperId = new Integer(item.get("PaperID").toString());
                        int authorId = new Integer(item.get("AuthorID").toString());
                        String authorName = (String) item.get("Name");
                        
                        // Get existing entity
                        Author author = this.authorMap.get(authorId);
                        
                        // if null create
                        if(author == null) {
                            author = new Author();
                            author.setAuthor_id(authorId);
                            author.setName(authorName);
                            
                            // Add to the map
                            this.authorMap.put(authorId, author);
                        }

                        // Get existing paper id if present
                        LinkedList<Author> authorList = this.paperAuthorLstMap.get(paperId);

                        // Add element to the map
                        if (authorList == null) {

                            // Create the list and add the first author to it
                            LinkedList<Author> list = new LinkedList<>();
                            list.add(author);
                            
                            // Add it to the map
                            this.paperAuthorLstMap.put(paperId, list);

                        } else {

                            // Add author to the paper
                            authorList.add(author);

                        }

                    }
                    // Show message
                    System.out.println("Importato il file " + jsonFile.getName());
                } catch (IOException | ParseException ex) {
                    Logger.getLogger(Shrink.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            System.out.println("Importing files ended");

        }
    }

    
    /**
     * Build the graph
     */
    private void buildGraph() {
        
        // Loop over map values
        Iterator it = this.paperAuthorLstMap.entrySet().iterator();
        while (it.hasNext()) {
           Map.Entry pair = (Map.Entry)it.next();
           LinkedList<Author> authorList = (LinkedList<Author>) pair.getValue();
           
           // Loop over elements
           for(int i = 0; i < authorList.size(); i++) {
               
               if(i+1 > authorList.size())
                   break;
               
               for(int j = i+1; j<authorList.size(); j++) {
                   graph.addLink(authorList.get(i), authorList.get(j));
               }
           }
           
           // avoids a ConcurrentModificationException
           it.remove(); 
           
        }
        
        System.out.println("Building graph ended");
    }
    
    private void clusterize() {
        System.out.println("Begin clustering");
        graph.clusterWithShrink();
        System.out.println("End clustering");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Shrink mainShrink = new Shrink();
        // Load data
        mainShrink.loadData(args);
        // Build the graph
        mainShrink.buildGraph();
        
        Date begin = new Date();
        
        // Cluster with shrink
        mainShrink.clusterize();
        
        Date end = new Date();
        

        long diffInMillies = end.getTime() - begin.getTime();
        System.out.println("Clustering performed in " + TimeUnit.MINUTES.convert(diffInMillies,TimeUnit.MILLISECONDS) + " minutes");
        System.out.println("Clustering performed in " + TimeUnit.SECONDS.convert(diffInMillies,TimeUnit.MILLISECONDS) + " seconds");

        
        System.out.println("Done");
    }

}
