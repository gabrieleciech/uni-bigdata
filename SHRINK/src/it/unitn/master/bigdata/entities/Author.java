/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.master.bigdata.entities;

//import java.util.LinkedList;
//import java.util.List;

/**
 *
 * @author gabrieleciech
 */
public class Author {
    
    private int author_id;
    private String name;
//    private List<Integer> writtenPapers = new LinkedList();
    
    /**
     * @return the author_id
     */
    public int getAuthor_id() {
        return author_id;
    }

    /**
     * @param author_id the author_id to set
     */
    public void setAuthor_id(int author_id) {
        this.author_id = author_id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

//    /**
//     * @return the writtenPapers
//     */
//    public List<Integer> getWrittenPapers() {
//        return writtenPapers;
//    }
//
//    /**
//     * @param writtenPapers the writtenPapers to set
//     */
//    public void setWrittenPapers(List<Integer> writtenPapers) {
//        this.writtenPapers = writtenPapers;
//    }
    
}
