#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function
import sys

for line in sys.stdin:
	print('1 %s' % line.strip().split()[0])
