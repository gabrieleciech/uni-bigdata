#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function
import sys

def main ():
	t = []
	with open(sys.argv[1], 'r') as ifp:
		for line in ifp.readlines():
			minutes, seconds = line.strip().split()[1].split(':')
			print('%f' % (int(minutes) * 60 + float(seconds)))

if __name__ == '__main__':
	main()
