/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.master.bigdata.utils;

/**
 *
 * @author gabrieleciech
 */
public class Parameters {

    /* This is the little uncertainty to attach to the epsilon measure when
       structural similarities are compared for micro-cluster detection (SHRINK).
       The intended semantics is (a==eps) iff (eps-deltaEps <= a <= eps), directionality important.
    */
    public static /*final*/ double param_deltaEps = 0.0;
    public static boolean is_epsilon_equal(double what, double eps) {
        return (eps-Parameters.param_deltaEps <= what) && (what <= eps);
    }
    /* Further shrinking steps after modularity is deemed as satisfactory enough */
    public static /*final*/ int SHRINK_STEPS_AFTER_MODULARITY = 0;

}
