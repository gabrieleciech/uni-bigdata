#!/bin/bash

if [ $# != 2 ]; then
	echo "usage: $0 <input> <output>"
	exit 1
fi

cmd=${HADOOP_HOME}/bin/hadoop
jar=${HADOOP_HOME}/share/hadoop/tools/lib/hadoop-streaming-2.7.0.jar

mapper=a-mapper.py
reducer=a-reducer.py

${cmd} jar ${jar} \
	-files ${mapper},${reducer} \
	-mapper ${mapper} -combiner ${reducer} -reducer ${reducer} \
	-input $1 -output $2
