#!/usr/bin/python
# encoding: utf-8

from __future__ import print_function
import sys
import json

def scan (filename):
	with open(filename, 'r') as ifp:
		for entry in json.load(ifp, 'utf-8').get('d').get('results'):
			yield(entry.get('PaperID'), entry.get('AuthorID'))

def digest (source, destination):
	with open(destination, 'w') as ofp:
		for entry in scan(source):
			print('%s %s' % (entry), file=ofp)

def main ():
	if len(sys.argv) != 3:
		raise Exception('Need two arguments: source and destination')
	digest(sys.argv[1], sys.argv[2])

if __name__ == '__main__':
	main()
