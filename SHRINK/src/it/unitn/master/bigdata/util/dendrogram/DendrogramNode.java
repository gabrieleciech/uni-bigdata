/*
 * This file is licensed to You under the "Simplified BSD License".
 * You may not use this software except in compliance with the License. 
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/bsd-license.php
 * 
 * See the COPYRIGHT file distributed with this work for information
 * regarding copyright ownership.
 */
/*******************************************************************************
 * Disclaimer: code adapted from
 *    https://github.com/sape/hac/tree/master/src/ch/usi/inf/sape/hac/dendrogram
 ******************************************************************************/
package it.unitn.master.bigdata.util.dendrogram;

import java.util.Collection;
import java.util.LinkedList;


/**
 * A DendrogramNode is a node in a Dendrogram.
 * It represents a subtree of the dendrogram tree.
 * It has two children (left and right), 
 * and it can provide the number of leaf nodes (ObservationNodes) in this subtree.
 */
public abstract class DendrogramNode {
        private static int countOfNodes = 0;
        private final int ID;
        public DendrogramNode() {
            this.ID = countOfNodes++;
        }
        public int getID() {
            return this.ID;
        }

	public abstract Collection<DendrogramNode> getMicroCluster();
	public abstract int getObservationCount();
        
        /* returns the observed values rooted by 'node' */
        public static Collection<Integer> getValuesUnder(DendrogramNode node) {
            Collection<Integer> res = new LinkedList<>();
            if (node==null) {
                /* do nothing */
            } else if (node instanceof ObservationNode) {
                ObservationNode o_node = (ObservationNode)node;
                res.add(o_node.getObservation());
            } else if (node instanceof MergeNode) {                
                for (DendrogramNode mergenode: node.getMicroCluster()) {
                    res.addAll(getValuesUnder(mergenode));                    
                }                  
            }
            return res;
        }
}