\section{Graph analysis with SHRINK}

We tried another clustering algorithm, called SHRINK. This clustering technique
was applied on a graph which was built using the data of the
\emph{Paper\_Author} table. Each node $v$ represents an author and there exists
a link $e$ between two nodes only if the two authors have written a paper
together. We cluster the nodes using [a slightly modified version of] the
SHRINK-H algorithm proposed in \cite{shrink}, whose general attributes we assume
known but recall briefly.

SHRINK is an agglomerative hierarchical clustering algorithm. Bottom-up it
iteratively finds micro-clusters in the network using a structural measure of
node aggregation (local connectivity), based on how much weight is put on edges.
Node aggregation is intended as if edges were physical cords, as much under
stretch as their weight says. Pictorially, nodes where the stretch is stiffer
form natural steadier conglomerations. The iterative procedure terminates when
the resulting clustering of nodes shows a degree of modularity which is
satisfactory.

As a matter of fact, SHRINK is the result of the combination of different
techniques used in the prior literature to solve the same clustering problem. As
such, it is able to grasp the strength of each while leaving the larger of their
drawbacks apart. Other good reasons to pick SHRINK as our node clustering
algorithm include its ability to keep \textit{outliers} (authors not relevant to
any other) and \textit{hubs} (authors too central in the field) off the clusters
and the promising experimental evaluations presented in \cite{shrink}, that
showed how versatile this framework is to adapt to any kind of networks. The
property of not being dependent on any parameter is also nicely acceptable.

Despite its general effectiveness, we found two possible shortcomings in the
application of SHRINK-H to our domain: first \textit{it shrinks too small
micro-clusters} and second \textit{it stops iteration too early}. The first
issue is the byproduct of non-parameterizing on a value, $\epsilon$, in the
definition of micro-clusters. In fact, for a micro-cluster to be larger than a
$\epsilon$-dense pair of nodes, it has to connect its nodes by \textit{exactly}
the same value of $\epsilon$. As a result, the algorithm keeps combining,
mostly, pairs of nodes, and the definition of micro-clusters fades pointless.
This is something that is already exhibiting in the original paper of SHRINK -
albeit never explicitly mentioned - when claiming equivalence between outputs of
the presented SHRINK-H and SHRINK-G algorithms in defining the final clusters.

In the matter of the second issue, namely that SHRINK \textit{stops iteration
too early}, that is something that shows up in particular kind of networks only,
as in the case of the \textit{Zarchary's Karate network} analysis in
\cite{shrink}. The problem is basically about modularity being too inaccurate as
a stopping criterion for the clustering. As brought back about by the authors
from the discussion in \cite{fortunato2007resolution}, modularity can give a
sub-optimal clustering (wrt the ground truth) even if its value on a particular
clustering is the global optimum on the network. This was the reason that lead
the authors to use it as a stopping criterion rather than as a driving force in
the clustering. However, in some cases it can still be too much outright to rely
on modularity for stopping.

In our version, which can be found in the \texttt{SHRINK/} folder, we extended
SHRINK to account for it: once the stopping criterion is met, we still make $c$
more shrinking iterations to complete the clustering. The parameter $c$ needs to
be carefully selected but, as it is nice to see, it can be greater than the true
optimum: thanks to the hierarchical structure of the SHRINK's clustering
process, a final cluster that is believed too large can be split in its
preceding micro-constituents. For obvious reasons of faithfulness to the
modularity measure, the value of $c$ should never exceed, say $5$. It can be set
to $0$ for equivalence to SHRINK.

\subsection{Performance issues}
Despite the improvements we made to the original algorithm, we abandoned the
idea. As we found, SHRINK is very demanding on memory size and we could not
manage to make it run inside our current setup. The processing of a single chunk
required $9GB$ and took more than 4 minutes while running on \emph{Sun}, which
is the most powerful and resourceful machine we have available. No experiment of
reasonable size could be performed under these circumstances.

