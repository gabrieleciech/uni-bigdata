#!/bin/bash

for target in ${@}; do
	destination=${target/.json/-short.json}
	echo "Target ${target} → ${destination}"
	./digest-author.py ${target} ${destination}
done
