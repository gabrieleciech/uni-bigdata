#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function
import sys

def main ():
	for line in sys.stdin:
		m, s = line.split()[1].split(':')
		print('%f' % (int(m) * 60 + float(s)))

if __name__ == '__main__':
	main()
