#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function
import sys

def parse_file (filename):
	with open(filename, 'r') as ifp:
		for line in ifp:
			paper, author = line.split()
			print('CREATE (:Paper {id:"%s"});' % paper)
			print('CREATE (:Author {id:"%s"});' % author)
			print('MATCH (a:Author),(p:Paper) WHERE a.name = "%s" AND b.name = "%s" CREATE (a)-[r:wrote]->(p) RETURN r' % (author, paper))

def main ():
	for name in sys.argv[1:]:
		parse_file(name)

if __name__ == '__main__':
	main()
