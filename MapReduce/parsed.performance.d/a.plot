#!/usr/bin/gnuplot -persistent

set logscale x
set logscale y

set title custom_title

set xlabel "# of entries"
set format x "10^%T"
set ylabel "Execution time, in seconds"
set format y "%.0fs"

set yrange [0.1:2000]
set xrange [9000:11000000]

#set terminal postscript eps enhanced color
#set output eps_file

plot dat_file using 1:7 with lines title 'serial Tanngnojstr', \
	'' using 1:4 with lines title 'serial DrDell', \
	'' using 1:6 with lines title 'Tanngnojstr', \
	'' using 1:3 with lines title 'DrDell', \
	'' using 1:2 with lines title 'Cluster', \
	'' using 1:5 with lines title 'Optimized Cluster'
