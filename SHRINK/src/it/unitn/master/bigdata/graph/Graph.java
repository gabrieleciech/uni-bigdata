/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.master.bigdata.graph;

import it.unitn.master.bigdata.entities.Author;
import it.unitn.master.bigdata.utils.Pair;
import it.unitn.master.bigdata.utils.Parameters;
import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Map;
import it.unitn.master.bigdata.util.dendrogram.Dendrogram;
import it.unitn.master.bigdata.util.dendrogram.DendrogramBuilder;
import it.unitn.master.bigdata.util.dendrogram.DendrogramNode;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

/**
 *
 * @author gabrieleciech
 */
public class Graph extends SimpleWeightedGraph<Author, DefaultWeightedEdge> {
    
    /* ---------------------------------- */
    /* ------- SHRINK Statistics -------- */
    private int cluster_replacements = 0;
    /* ---------------------------------- */
    
    /**
     * Constructor
     */
    public Graph() {
        super(DefaultWeightedEdge.class);
    }

    /**
     * Connects Author1 to Author2. If link not present sets
     * the weight of the edge to 1 else adds one to the current weight
     * @param author1 first author
     * @param author2 second author
     */
    public void addLink(Author author1, Author author2) {
        this.addVertex(author1);
        this.addVertex(author2);
        
        DefaultWeightedEdge existingEdge = this.getEdge(author1, author2);
        
        if (existingEdge == null) {
            // Add edge with default weight 1
            try {
                DefaultWeightedEdge edge = this.addEdge(author1, author2);
                this.setEdgeWeight(edge, 1);
            } catch(Exception e) {
                System.out.println("Self-edge not added\n"
                        + "\t" + author1.getAuthor_id() + " - " + author1.getName()
                        + "\n"
                        + "\t" + author2.getAuthor_id() + " - " + author2.getName());
                System.out.flush();
            }
        } else {
            // Get the weight of the current one and modify it
            double weight = this.getEdgeWeight(existingEdge);
            weight += 1;
            this.setEdgeWeight(existingEdge, weight);
        }
    }
    
    public Dendrogram clusterWithShrink() {
        Dendrogram result;        
        DendrogramBuilder dendrogramBuilder = new DendrogramBuilder(this);

        boolean time_to_exit = false;
        boolean exit_countdown_started = false;
        int exit_countdown = Parameters.SHRINK_STEPS_AFTER_MODULARITY;
        for (int shrink_iteration=0; !time_to_exit; shrink_iteration++) {
            LinkedList<Pair<Double,LinkedList<DendrogramNode>>> MC = new LinkedList();
            /* Phase 1: Detect local micro-communities */
            System.out.println("Entering Phase 1 of SHRINK - iteration #" + shrink_iteration);
            boolean ntran = true; // need to reconsider all nodes
            while (ntran) {
                ntran=false;
                for (DendrogramNode v: dendrogramBuilder.getNodes()) {
//System.out.println("Process node "+v.getID()+". The node is "+((already_in_some_cluster(v,MC))?"":"not ")+"in some other cluster");
//System.out.println(v);
                    if (!already_in_some_cluster(v,MC)) { /* we don't want to start from a node in another cluster (notice that eps is lower for it) */
                        LinkedList<DendrogramNode> C_v = new LinkedList();
                        ArrayDeque<DendrogramNode> q = new ArrayDeque<>();
                        q.addLast(v);
                        Map edgesFromvTo = dendrogramBuilder.getCurrentEdgesFrom(v);
                        double eps = maxout(edgesFromvTo);

                        while (!q.isEmpty()) {
                            DendrogramNode u = q.removeFirst();
//System.out.println("Dequeue node "+u.getID()+". The node is "+((C_v.contains(u))?"":"not ")+"in C_v");
//System.out.println(u);                        
                        
                            Map<DendrogramNode, Double> edgesFromuTo = dendrogramBuilder.getCurrentEdgesFrom(u);
                            if ((u==v) || ((Parameters.is_epsilon_equal(maxout(edgesFromuTo), eps))
                                    && (!already_in_some_cluster_with_ge_eps(u,MC,eps))
                                    )) {
                                if (already_in_some_cluster(u,MC)) {
                                    /* in this case it is in another cluster with an
                                     * eps value which is less than the current eps,
                                     * so we may reset that cluster to the original
                                     * configuration, pretending it was never seen.
                                     */
                                    assert (u!=v);
                                    System.out.println("--- Cluster replacement #"+(++cluster_replacements)+" occurring ---");
                                    free_MC_from_cluster_with_u(u,MC); // has a side effect on MC!
                                    ntran = true;
                                }
                                C_v.add(u);
//System.out.println("Dequeued node "+u.getID()+" is added to C_v"); System.out.println();
                                for (Map.Entry<DendrogramNode, Double> entry: edgesFromuTo.entrySet()) {
                                    DendrogramNode other_endpoint = entry.getKey();
                                    double w = entry.getValue();
                                    if (Parameters.is_epsilon_equal(w,eps)) {
                                        if ((!C_v.contains(other_endpoint)) && (!q.contains(other_endpoint))) {
                                            q.addLast(other_endpoint);
                                        }                                    
                                    }
                                }
                            }
                        }
                        MC.add(new Pair(eps,C_v));
//System.out.println("-------------- FINAL CLUSTER: --------------"); for (DendrogramNode node: C_v) System.out.println(node); System.out.println("----------------------------------------------");
                    }
                }
            }
//for (LinkedList<DendrogramNode> cluster: MC) { System.out.println("---------- Cluster ----------"); for (DendrogramNode node: cluster) System.out.println(node); System.out.println("-----------------------------\n");}
            /* Phase 2: Shrink micro-communities */
            System.out.println("Entering Phase 2 of SHRINK - iteration #" + shrink_iteration);
            boolean delta_Qs = false;
            for (Pair<Double,LinkedList<DendrogramNode>> cluster: MC) {
                LinkedList<DendrogramNode> cluster_nodes = cluster.right();
                double cluster_eps = cluster.left();
                double deltaQs_cluster = dendrogramBuilder.computeDeltaGain(cluster_nodes);
                if ((cluster_nodes.size()>1) && (deltaQs_cluster>0.0)) {
                    dendrogramBuilder.shrinkNodes(cluster_nodes, cluster_eps);
                    delta_Qs = true;                    
                }
            }
            if (delta_Qs) dendrogramBuilder.adjustWeights();
            else exit_countdown_started = true; // yes, sooner or later this will happen            
            
            //assert (!exit_countdown_started || delta_Qs); // i.e. delta_Qs -> G delta_Qs // FALSE!
            if (exit_countdown_started) time_to_exit = (exit_countdown--==0);
        }
        
        result = dendrogramBuilder.getDendrogram();
        return result;
    }
    
    /* Builds clusters using the SHRINK algorithm based on weights in 'this'.
     * Note1: since SHRINK originally uses structural similarity, weights are
     *        supposed to be those of structural similarity (see also the
     *        related method buildStructuralSimilarityGraph(...))
     * Note2: this code is a bit different than SHRINK: in particular it uses a
     *        small range noise-tollerant epsilon parameter to build micro-comms
     *        and possibly iterates after the modularity check says stop
     */ 
    private double maxout(Map<DendrogramNode, Double> edgesTo) { /* returns the maximum outgoing edge weight */
        double res=0.0;
        for (double w: edgesTo.values()) {            
            if (w>res) res = w;
        }
        return res;
    }
    private boolean already_in_some_cluster(DendrogramNode node,
                                            LinkedList<Pair<Double,LinkedList<DendrogramNode>>> MC
                                            ) { /* Tells whether flatten(MC) already contains node with a greater value than eps */
        for (Pair<Double,LinkedList<DendrogramNode>> cluster: MC) {
            if (cluster.right().contains(node))
                return true;
        }
        return false;
    }
    private boolean already_in_some_cluster_with_ge_eps(DendrogramNode node,
                                                        LinkedList<Pair<Double,LinkedList<DendrogramNode>>> MC,
                                                        double eps
                                                        ) { /* Tells whether flatten(MC) already contains node */
        for (Pair<Double,LinkedList<DendrogramNode>> cluster: MC) {
            if (cluster.right().contains(node))
                return cluster.left() >= eps;
        }
        return false;
    }
    private void free_MC_from_cluster_with_u (DendrogramNode node,
                                              LinkedList<Pair<Double,LinkedList<DendrogramNode>>> MC
                                             ) /* SIDE EFFECT EXPECTED ON MC!!! */ {
        Pair<Double,LinkedList<DendrogramNode>> target = null;
        for (Pair<Double,LinkedList<DendrogramNode>> cluster: MC) {
            if (cluster.right().contains(node)) {
                target = cluster;
                break; // only one allowed
            }
        }
        assert (target != null); // it must have been contained one!
        assert (MC.remove(target));
    }
    
}
