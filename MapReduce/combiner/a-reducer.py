#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function
import sys

prev = None
for line in sys.stdin:
	word = line.strip().split()[0]
	if prev != word:
		print('%s' % word)
		prev = word
