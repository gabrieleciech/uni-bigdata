#!/usr/bin/python
# encoding: utf-8

from __future__ import print_function
from cassandra.cluster import Cluster, OperationTimedOut
import sys
import json

def parse_json_source (filename):

	with open(filename, 'r') as ifp:
		decoded = json.load(ifp, 'utf-8')

	for entry in decoded.get('d').get('results'):
		yield(entry.get('PaperID'), entry.get('AuthorID'), entry.get('SeqID'))

def main():

	cluster = Cluster()
	session = cluster.connect('mas')
	insert_stmt = session.prepare('INSERT INTO paper_author (paper, author, seq) VALUES (?, ?, ?);')
	count_stmt = session.prepare('SELECT COUNT(*) FROM paper_author')

	for name in sys.argv[1:]:

		for entry in parse_json_source(name):
			while True:
				try:
					session.execute(insert_stmt, entry)
					break
				except OperationTimedOut:
					print('%s timed out, retrying…')
		print('%s done' % name)

	try:
		print(session.execute(count_stmt))
	except OperationTimedOut:
		print('No count available')

if __name__ == '__main__':
	main()
