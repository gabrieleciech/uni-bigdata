#!/usr/bin/gnuplot -persistent

set logscale x
set logscale y

set xlabel 'Community size'
set format x '10^%T'
set ylabel '# of communities'
set format y '10^%T'

set xrange [0.9:2000000]
set yrange [0.9:1000000]

set terminal postscript eps enhanced color
set output 'community.eps'

plot 'h2.histo' title '2nd level' with points pointtype 7 pointsize 1, \
	'h3.histo' title '3rd level' with points pointtype 7 pointsize 1, \
	'h4.histo' title '4th level' with points pointtype 7 pointsize 1, \
	'h8.histo' title '8th level' with points pointtype 7 pointsize 1
