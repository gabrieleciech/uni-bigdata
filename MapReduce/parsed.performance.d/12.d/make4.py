#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function
import sys

def main ():
	t = []
	with open(sys.argv[1], 'r') as ifp:
		for line in ifp.readlines():
			minutes, seconds = line.strip().split()[1].split(':')
			t.append(int(minutes) * 60 + float(seconds))

	c = 0.0
	for i in xrange(0,12):
		c += t[i]
		if i % 3 == 2:
			print('%f' % (c / 3))
			c = 0.0

if __name__ == '__main__':
	main()
