#!/usr/bin/gnuplot -persistent

set logscale x
set logscale y

set xlabel "# of entries"
set format x "10^%T"
set ylabel "Execution time, in seconds"
set format y "%.0fs"

set xrange [9000:80000000]
set yrange [0.05:5000]

set terminal postscript eps enhanced color
set output 'search.eps'

plot 'louvain.dat' using 1:3 with lines title "DrDell",\
	'luna.dat' using 1:3 with lines title "Luna"
