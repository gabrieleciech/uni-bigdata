/*
 * This file is licensed to You under the "Simplified BSD License".
 * You may not use this software except in compliance with the License. 
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/bsd-license.php
 * 
 * See the COPYRIGHT file distributed with this work for information
 * regarding copyright ownership.
 */
/*******************************************************************************
 * Disclaimer: code adapted from
 *    https://github.com/sape/hac/tree/master/src/ch/usi/inf/sape/hac/dendrogram
 ******************************************************************************/
package it.unitn.master.bigdata.util.dendrogram;

import java.util.Collection;


/**
 * An ObservationNode represents a leaf node in a Dendrogram.
 * It corresponds to a singleton cluster of one observation.
  */
public final class ObservationNode extends DendrogramNode {
	private final Integer observation;

	public ObservationNode(final Integer observation) {
                super();
		this.observation = observation;
	}

        @Override
        public String toString() {
            return "ObservationNode{" + "observation=" + observation + '}';
        }        
        
        @Override
	public final Collection<DendrogramNode> getMicroCluster() {
		return null;
	}

        @Override
	public int getObservationCount() {
            return 1;
	}
	
	public final Integer getObservation() {
		return observation;
	}
}