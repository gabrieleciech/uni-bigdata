/*
 * This file is licensed to You under the "Simplified BSD License".
 * You may not use this software except in compliance with the License. 
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/bsd-license.php
 * 
 * See the COPYRIGHT file distributed with this work for information
 * regarding copyright ownership.
 */
/*******************************************************************************
 * Disclaimer: code adapted from
 *    https://github.com/sape/hac/tree/master/src/ch/usi/inf/sape/hac/dendrogram
 ******************************************************************************/
package it.unitn.master.bigdata.util.dendrogram;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Collection;




/**
 * A Dendrogram represents the results of hierachical agglomerative clustering.
 * The root represents a single cluster containing all observations.
 */
public final class Dendrogram {
    private final DendrogramNode root;

    public Dendrogram(final Collection<DendrogramNode> nodes) {
        this.root = new MergeNode(nodes, 0.0);  // this is the coerced final merge
    }

    public DendrogramNode getRoot() {
        return this.root;
    }

    public void dump(BufferedWriter bw) throws IOException {
        bw.write(this.root+"\n");
    }

    public Collection<Integer>[] getFinalDivision() {
        Collection<Integer>[] res = new Collection[this.root.getMicroCluster().size()];
        int i=0;
        for (DendrogramNode node: root.getMicroCluster()) {
            res[i] = DendrogramNode.getValuesUnder(node);
            i++;
        }
        return res;
    }
    
}