/*
 * This file is licensed to You under the "Simplified BSD License".
 * You may not use this software except in compliance with the License. 
 * You may obtain a copy of the License at
 *
 * http://www.opensource.org/licenses/bsd-license.php
 * 
 * See the COPYRIGHT file distributed with this work for information
 * regarding copyright ownership.
 */
/*******************************************************************************
 * Disclaimer: code adapted from
 *    https://github.com/sape/hac/tree/master/src/ch/usi/inf/sape/hac/dendrogram
 ******************************************************************************/
package it.unitn.master.bigdata.util.dendrogram;

import it.unitn.master.bigdata.entities.Author;
import it.unitn.master.bigdata.graph.Graph;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import org.jgrapht.graph.DefaultWeightedEdge;



/**
 * A DendrogramBuilder creates a Dendrogram consisting of ObservationNodes and
 * MergeNodes.
 */
public final class DendrogramBuilder {
    private Collection<DendrogramNode> nodes;
    public Collection<DendrogramNode> getNodes() {
        return this.nodes;
    }
    private Collection<DendrogramNode> freshNodes; /* freshly shrunk nodes */
    
    /* List of edges with weights. Indices indicate nodeFrom's IDs.*/
    /* Edges of all levels in the dendrogram are kept and never removed, for
     * consistency throughout the dendrogram. We provide three getters:
     *   - getEdgesFrom() and getAllEdgesFrom() do the same thing of returning
     *      the complete list of edges kept by the class instance.
     *   - getCurrentEdgesFrom() only returns edges of the current graph
     *      (i.e. current level of the dendrogram being built).
     */
    private ArrayList<Map<DendrogramNode,Double>> edgesFrom;
    public Map<DendrogramNode, Double> getAllEdgesFrom(DendrogramNode which) {
        return edgesFrom.get(which.getID());
    }
    public Map<DendrogramNode, Double> getEdgesFrom(DendrogramNode which) {
        return this.getAllEdgesFrom(which);
    }
    public Map<DendrogramNode, Double> getCurrentEdgesFrom(DendrogramNode node1) { // yeah, it could be defined more statically...
        assert (this.nodes.contains(node1)); // both edges' endpoints must be there in the current graph!
        Map<DendrogramNode, Double> all_edges = edgesFrom.get(node1.getID());
        assert (all_edges instanceof LinkedHashMap); // ensure we are coherent with types
        Map<DendrogramNode, Double> res_edges = new LinkedHashMap();
        
        for (Map.Entry<DendrogramNode, Double> entry: all_edges.entrySet()) {
            DendrogramNode node2 = entry.getKey();
            double weight = entry.getValue();
            if (this.nodes.contains(node2)) res_edges.put(node2, weight);
        }
        
        return res_edges;
    }
    
    
    
    /* Modularity info */
    private final double TS;
    private ArrayList<Double> DSiFrom;
    private ArrayList<Map<DendrogramNode,Double>> USijFrom;
    /* --------------- */

    
    
    /* 'pg_model' is supposed to be a complete graph of structural similarities */
    public DendrogramBuilder(final Graph pg_model) {
        System.out.println("Numero nodi:     " + pg_model.vertexSet().size());
        System.out.println("Numero di archi: " + pg_model.edgeSet().size());
        
        HashMap<Integer, Author> vertex_map = new HashMap<>();
        for(Author a: pg_model.vertexSet()) {
            vertex_map.put(a.getAuthor_id(), a);
        }
        
        int nObservations = vertex_map.size();
        /* Set up the initial observation nodes */
        nodes = new HashSet();
        for(Integer i : vertex_map.keySet()) {
            DendrogramNode observationNode = new ObservationNode(i);
            nodes.add(observationNode);
        }
        
        /* Set up the initial edges, with weights and modularity information */
        this.edgesFrom = new ArrayList(nObservations); // this is for performance only        
        double TS_ = 0.0;
        this.DSiFrom   = new ArrayList(nObservations); // this is for performance only        
        this.USijFrom  = new ArrayList(nObservations); // this is for performance only       
        for (int i=0; i<nObservations; i++) {          // preallocate memory
            this.edgesFrom.add(null);
            this.DSiFrom.add(null);
            this.USijFrom.add(null);
        }
        
        // Loop over nodes
        for (DendrogramNode nodeFrom: nodes) {
            
            Map<DendrogramNode,Double> nodeTos = new LinkedHashMap();
            double DSi_ = 0.0;
            
            for (DendrogramNode nodeTo: nodes) {
                if (nodeFrom != nodeTo) {
                    ObservationNode obsNodeFrom = (ObservationNode) nodeFrom;
                    ObservationNode obsNodeTo = (ObservationNode) nodeTo;
                    Author p1 = vertex_map.get(obsNodeFrom.getObservation());
                    Author p2 = vertex_map.get(obsNodeTo.getObservation());
                    DefaultWeightedEdge edge = pg_model.getEdge(p1, p2);
                    // Edge dows not exists
                    if (edge == null) {
                        continue;
                    }
                    double weight = pg_model.getEdgeWeight(edge);
                    nodeTos.put(nodeTo, weight);
                    if (nodeFrom.getID() < nodeTo.getID()) TS_ += weight;
                    DSi_ += weight;
                }
            }
            this.edgesFrom.set(nodeFrom.getID(), nodeTos);
            this.DSiFrom.set(nodeFrom.getID(), DSi_);
            this.USijFrom.set(nodeFrom.getID(), nodeTos);
        }
        this.TS = TS_;
        
        /* Assign each observation node a specific cluster and adjust (reset) weights */
        for (DendrogramNode observationNode: new LinkedList<>(nodes)) {
            assert(observationNode instanceof ObservationNode);
            LinkedList<DendrogramNode> observationCluster = new LinkedList(); // singleton!
            observationCluster.add(observationNode);
            this.shrinkNodes(observationCluster, 1.0);
        }
        this.adjustWeights();
        
    }

    
    
    /* Computes the modularity dalta gain of a potential new cluster.
       It is not computed the true delta gain but only a simplification that
       mantains validity under the preticate of being positive, i.e.
       (true_modularity_deltaGain > 0) => (simplified_modularity_deltaGain > 0)
    */
    public double computeDeltaGain(Collection<DendrogramNode> microCluster) {
        double sum_USij = 0.0;
        double sum_DSiDSj = 0.0;
        for (DendrogramNode node1: microCluster) {
            double DSi = this.DSiFrom.get(node1.getID());
            Map<DendrogramNode,Double> USijFromiTo = this.USijFrom.get(node1.getID());
            for (DendrogramNode node2: microCluster) {
                if (node1.getID() < node2.getID()) {
//System.out.println("AAAAAAAAAAAAAAAA:\t"+USijFromiTo.size());
//System.out.println("BBBBBBBBBBBBBBBB:"); this.dump_node_debug(node1);
//System.out.println("CCCCCCCCCCCCCCCC:"); this.dump_node_debug(node2);
//System.out.println("DDDDDDDDDDDDDDDD:\t"+USijFromiTo.get(node2));
                    double DSj = this.DSiFrom.get(node2.getID());
                    double USij = USijFromiTo.get(node2);
                    sum_USij += USij;
                    sum_DSiDSj += DSi*DSj;
                }
            }
        }

        return this.TS*sum_USij - sum_DSiDSj;
    }
    
    /* Shrinks a micro-cluster. The structural graph is assumed not to be needed
       steady when the merge occurs, because this function modifies the graph.
       This can simply be achieved by keeping phase 1 and phase 2 separated
       as in the paper of SHRINK. Right after the shrinking function has been
       called on all micro-clusters, the 'adjustWeights' function must follow.
       These need to be kept separate but the latter called as soon as the total
       shrinking of micro-clusters is finished. The latter also sets up the
       new modularity sufficient information (DSi, DSj and USij for all i,j).
    */
    private boolean firstShrink = true; // reset when weights are adjusted
    public final void shrinkNodes(final LinkedList<DendrogramNode> microCluster,
                                  final double shrinking_eps
                                 ) {
        if (this.firstShrink) {
            assert (this.freshNodes == null);
            this.freshNodes = new LinkedList();
        }

        final MergeNode newnode = new MergeNode(microCluster, shrinking_eps);
        for (DendrogramNode node: microCluster) { this.nodes.remove(node); }        
        this.freshNodes.add(newnode);
        this.firstShrink = false;
    }
    public final void adjustWeights(){
        assert (this.freshNodes != null);
        this.edgesFrom.ensureCapacity(this.edgesFrom.size()+this.freshNodes.size()); // just for performance
        this.USijFrom.ensureCapacity(this.USijFrom.size()+this.freshNodes.size()); // just for performance
        for (int i=0; i<this.freshNodes.size(); i++) {   // preallocate memory
            this.edgesFrom.add(null);
            this.DSiFrom.add(null);
            this.USijFrom.add(null);
        }        
        
        /* adjust edges from fresh nodes to already existing nodes */
        for (DendrogramNode freshNode: this.freshNodes) {
            Map<DendrogramNode,Double> freshEdgesTo = new LinkedHashMap();
            Map<DendrogramNode,Double> freshUSijTo = new LinkedHashMap();
            for (DendrogramNode node: this.nodes) {
//System.out.println("CHECK THIS OUT:\t\t"+freshNode.getID()+" and "+node.getID());
                /* Notice that nodes and fresh nodes are disjuncted sets */
                double max = 0.0;
                double USij = 0.0;
                for (DendrogramNode clusternode: freshNode.getMicroCluster()) {
                    /* Notice that nodes and clusternodes are disjuncted sets */
                    Map<DendrogramNode,Double> edgesTo = this.edgesFrom.get(clusternode.getID());                    
                    Map<DendrogramNode,Double> USijTo = this.USijFrom.get(clusternode.getID());                    
                    double candidate = edgesTo.get(node);
                    if (candidate > max) max = candidate;
                    /* Note: existing edges with weights need to be not removed! */                    
                    USij += USijTo.get(node);
                }
                this.edgesFrom.get(node.getID()).put(freshNode, max);
                freshEdgesTo.put(node, max);
                this.USijFrom.get(node.getID()).put(freshNode, USij);
                freshUSijTo.put(node, USij);
            }
            // GeneralUtil.symbolLine('-', 80);
            // System.out.println(freshEdgesTo);
            // this.dump_node_debug(freshNode);
            // GeneralUtil.symbolLine('-', 80);
            // System.exit(0);
            assert(this.edgesFrom.get(freshNode.getID()) == null);
            this.edgesFrom.add(freshNode.getID(), freshEdgesTo);
            /* Note: DSiFrom does *not* have to be updated here */
            assert(this.USijFrom.get(freshNode.getID()) == null);
            this.USijFrom.add(freshNode.getID(), freshUSijTo);
        }
        /* adjust edges from fresh nodes to fresh nodes */
        for (DendrogramNode freshNode1: this.freshNodes) {
            for (DendrogramNode freshNode2: this.freshNodes) {
                /* set some order to avoid to reconsider pairs twice (and avoid them equal) */
                if (freshNode1.getID() < freshNode2.getID()) {
                    double max = 0.0;
                    double USij = 0.0;
                    for (DendrogramNode clusternode1: freshNode1.getMicroCluster()) {
                        Map<DendrogramNode,Double> edgesTo1 = this.edgesFrom.get(clusternode1.getID());
                        Map<DendrogramNode,Double> USijTo1 = this.USijFrom.get(clusternode1.getID());
                        for (DendrogramNode clusternode2: freshNode2.getMicroCluster()) {
//System.out.println("1111111111111111:"); this.dump_node_debug(freshNode1);
//System.out.println("2222222222222222:"); this.dump_node_debug(freshNode2);
//System.out.println("BBBBBBBBBBBBBBBB:"); this.dump_node_debug(clusternode1);
//System.out.println("CCCCCCCCCCCCCCCC:"); this.dump_node_debug(clusternode2);
//System.out.println("DDDDDDDDDDDDDDDD:\t"); System.out.println(edgesTo1.get(clusternode2));
                            if(edgesTo1.get(clusternode2) == null)
                                continue;
                            double candidate = edgesTo1.get(clusternode2);
                            if (candidate > max) max = candidate;
                            /* Note: existing edges with weights need to be not removed! */
                            USij += USijTo1.get(clusternode2);
                        }
                    }
                    /* Note: both already have something set in this.edgesFrom (they're not null valued) */
                    this.edgesFrom.get(freshNode1.getID()).put(freshNode2, max);
                    this.edgesFrom.get(freshNode2.getID()).put(freshNode1, max);
                    /****************************************************/
                    /* Note: DSiFrom does *not* have to be updated here */
                    /****************************************************/
                    /* Note: both already have something set in this.USijFrom (they're not null valued) */
                    this.USijFrom.get(freshNode1.getID()).put(freshNode2, USij);
                    this.USijFrom.get(freshNode2.getID()).put(freshNode1, USij);
                }   
            }
            //GeneralUtil.symbolLine('-', 80);
            //System.out.println(this.edgesFrom.get(freshNode1.getID()));
            //this.dump_node_debug(freshNode1);
            //GeneralUtil.symbolLine('-', 80);
            // System.exit(0);

        }
        /* calculate the modularity information DSi-s separately  */
        for (DendrogramNode freshNode: this.freshNodes) {
            double sumDSi_ = 0.0;
            double sumUSij_ = 0.0;
            for (DendrogramNode clusternode_i: freshNode.getMicroCluster()) {
                sumDSi_ += this.DSiFrom.get(clusternode_i.getID());
                Map<DendrogramNode,Double> USijFromiTo = this.USijFrom.get(clusternode_i.getID());
                for (DendrogramNode clusternode_j: freshNode.getMicroCluster()) {                    
                    if (clusternode_i.getID() < clusternode_j.getID()) {
                        sumUSij_ += USijFromiTo.get(clusternode_j);
                    }
                }
            }
            
            assert(this.DSiFrom.get(freshNode.getID()) == null);
            double DSi = sumDSi_ - 2*sumUSij_;
            this.DSiFrom.add(freshNode.getID(), DSi);
        }
        
                    
        
        /* fix up what's left */
        this.nodes.addAll(this.freshNodes);
        this.freshNodes = null;
        this.firstShrink = true; // this is a reset operation, sorry for the misleading name
    }

    /* Return the final dendogram once the building process is finished */
    public final Dendrogram getDendrogram() {
        return new Dendrogram(nodes);
    }

    private void dump_node_debug(DendrogramNode node) {
        System.out.println("Node ID = " + node.getID());
        System.out.println(node);
    }
}