### a.sh - Authors ###

usage: cat <input> | ./a.sh | wc -l

Counts the number of unique authors

### p.sh - Papers ###

usage: cat <input> | ./p.sh | wc -l

Counts the number of unique papers

### ppa.sh - Papers Per Author ###

usage: cat <input> | ./ppa.sh | less

Counts the number of papers written by each author

### app.sh - Authors Per Paper ###

usage: cat <input> | ./app.sh | less

Counts the number of people authoring each paper
