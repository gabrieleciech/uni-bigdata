\section{MapReduce}

In this section we describe our approach to MapReduce. Two versions were
implemented -- one serial and one parallel based on Hadoop -- to extract various
features from the Paper\_Authors table. We performed some experiments to test
the consistency of the results and also the performance of the different
approaches.

\subsection{Operations}
Our effort was based on the digested version of the Paper\_Author table. We
decided to compute the number of papers and author listed in the table, as long
as the amount of papers written by each author and the amount of people
authoring each paper.

\begin{description}
	\item[A - Authors:] this operation consist in extracting the AuthorID field
	from each line in the input and return the list of unique IDs;
	\item[P - Papers:] similarly to the previous one, this operations consist in
	extracting the PaperID field from each line in the input and return the list
	of unique IDs;
	\item[PPA - Papers Per Author:] the AuthorID field alone is extracted from
	the input, but this time the values are grouped by each ID and the returned
	result is the count of elements associated to each;
	\item[APP - Authors Per Paper:] the PaperID field is extracted from the
	input, the entries are grouped by the same value and the count of each set
	is returned as output;
\end{description}

\subsection{Serial implementation}

Our first implementation was serial, consisting in a simple ollection of BASH
scripts. UNIX commands \texttt{cut}, \texttt{sort} and \texttt{uniq} were used
to perform the operations. The \texttt{cut} command extracts the selected input,
emulating the mapping phase; the \texttt{sort} command orders the tuples and
provides them to the \texttt{uniq} command, which acts as the reducer.

Sources can be found in the \texttt{MapReduce/serial/} folder. Each script
expects to receive data via \texttt{stdin} and provides results directly to
\texttt{stdout}.

\subsection{Parallel implementation}

The parallel version is written in \emph{Python} and works in combination with
the streaming APIs provided by \emph{Apache Hadoop}. Each implementation is
composed of a map function and of a reduce function, which are required to be
stored on separate files.

Due to how the Hadoop streaming process works, both function must read from
\texttt{sys.stdin} and print on \texttt{sys.stdout}; there are special
requirements, nor \emph{emit} operations provided to the scripts, as the system
assumes each line to be a tuple. We provide all the functions in the
\texttt{MapReduce/parallel/} folder, along with BASH scripts to perform the
invocation.

They work in a similar as their respective serial versions, but they require the
\texttt{HADOOP\_HOME} environment variable to be set and the input and output
directories as arguments. If Hadoop is set to run as standalone, the two
arguments will be interpreted as local paths; if Hadoop is set to run as a
one-node virtual cluster, the two arguments will instead be interpreted as paths
on HDFS.

\subsection{Minor optimization}

All four mapping operations produce a substantial amount of tuples which may be
duplicated, and all could benefit from the optional combining phase provided by
MapReduce.

The first two operations – \texttt{a, p} – can be executed with the addition of
the optional step without modification, but the last two operations –
\texttt{ppa, app} – require an additional counter.

Sources for these optimized versions can be found in the
\texttt{MapReduce/combiner/} folder. They can be invoked in the same fashion as
their parallel counterparts.

\subsection{Experimental environment}

Our initial plan was to run a batch of experiments on Amazon \emph{EMR – Elastic
Map Reduce}, which provides a fast and simple web interface to create clusters,
ran tasks and collect results and statistics. Unfortunately, the service does
not allow the usage of \texttt{t2.micro} instance – due to their limited
resources, these instances are not up to the task to run Hadoop – and thus we
could not use access to it with our trial account. Instead, we build a tiny
local cluster and tested it against other smaller configuration.
\emph{Tanngnojstr} and \emph{DrDell} were employed for this task.

The serial batch of experiments was run on both machines. The parallel batch was
tested on both machines running \emph{Apache Hadoop} v\texttt{2.7.0} in three
different configuration: first as a standalone application, then as a
single-node cluster and finally as a proper two-nodes cluster. The optimized
batch was tested only on the cluster.

The standalone behaviour was achieved by running Hadoop with no configuration at
all.  The single-node Hadoop configuration files can be found in the
\texttt{MapReduce/single-node/} folder, while the two-nodes cluster
configuration file in the \texttt{MapReduce/cluster-nodes/} folder instead. The
last configuration uses hostnames and is supposed to run on a safe network where
all hosts are known to all, and where SSH connections can be established without
inserting passwords.

We run a set of experiments for each operation. Each instance was repeated three
times, and its performance averaged. Scalability and time performance was
analyzed by sampling different subsets of the dataset, using sets of 1, 2, 4,
10, 20, 40, 100, 200, 400 and 1000 chunks of data as input, each containg 10000
edges.

\subsection{Experimental results}

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{src/plot/a.eps}
	\caption{Time performance of \emph{Author} task}
	\label{task:a}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{src/plot/p.eps}
	\caption{Time performance of \emph{Paper} task}
	\label{task:p}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{src/plot/app.eps}
	\caption{Time performance of \emph{Authors per Paper} task}
	\label{task:app}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{src/plot/ppa.eps}
	\caption{Time performance of \emph{Papers per Author} task}
	\label{task:ppa}
\end{figure}

Figures~\ref{task:a},~\ref{task:p},~\ref{task:app} and~\ref{task:ppa} show the
time perfomance of the various tasks running on different hardware
configurations. Results show clearly how limited our clusters are with respect
to the serial version of each tasks, which completes ten times faster.

In all configuration, \emph{DrDell} executes tasks faster than
\emph{Tanngnojstr}, having better hardware overall. The two-nodes cluster,
despite having access to more resources, manages to run faster than single-node
clusters only in a few occasions, most likely suffering from network
communication overhead.

The replication parameter value for HDFS was initially set to 1 for all
configurations. The performance for the two-nodes cluster fell behind all the
others, so we set the value to 2 for better disk usage, thus achieving the
results shown in the figures.

On both machines, the standalone version and the single-node cluster version had
extremely similar execution times and the displayed lines were
indistinguishable. We decided to omit them, for better readability.

The optimized version of each task, which we tested exclusively on the two-nodes
cluster, shows no performance increase with respect to the non-optimized
version, and falls behind as the input size increases. This may explained by the
lack of resources due to the limited number of machines in the cluster, but it
may also be related to some odd data distribution in the input.
