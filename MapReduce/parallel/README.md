### a.sh - Authors ###

usage: HADOOP\_HOME=<hadoop_home_path> | ./a.sh <input> <output>

Counts the number of unique authors

### p.sh - Papers ###

usage: HADOOP\_HOME=<hadoop_home_path> | ./p.sh <input> <output>

Counts the number of unique papers

### ppa.sh - Papers Per Author ###

usage: HADOOP\_HOME=<hadoop_home_path> | ./ppa.sh <input> <output>

Counts the number of papers written by each author

### app.sh - Authors Per Paper ###

usage: HADOOP\_HOME=<hadoop_home_path> | ./app.sh <input> <output>

Counts the number of people authoring each paper
