#!/bin/bash

if [ $# -le 1 ]; then
	echo "Usage: $0 <logfile> <input>"
	exit 1
fi

logfile=$1
shift

for i in ./a.sh ./p.sh ./app.sh ./ppa.sh; do
	echo "Invoking $i…"
	/usr/bin/time -o ${logfile} -a cat $@ | ${i} > /dev/null
done
